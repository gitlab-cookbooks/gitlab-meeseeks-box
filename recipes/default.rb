# encoding: UTF-8

# Cookbook Name:: meeseeks-box
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.
require 'yaml'

include_recipe 'chef-vault'

vault_data = chef_vault_item(node['meeseeks-box']['chef_vault'],
                node['meeseeks-box']['chef_vault_item'])

all_data = {}.merge(node).merge(vault_data)

apt_repository 'gitlab-aptly' do
  uri node['gitlab-aptly']['uri']
  distribution node['gitlab-aptly']['distribution']
  components node['gitlab-aptly']['components']
  arch node['gitlab-aptly']['arch']
  key node['gitlab-aptly']['key']
  cache_rebuild node['gitlab-aptly']['cache_rebuild']
  trusted node['gitlab-aptly']['trusted']
end

apt_package node['meeseeks-box']['package'] do
  version node['meeseeks-box']['version']
  action :upgrade
end

service 'meeseeks-box' do
  action :nothing
end

template node['meeseeks-box']['env']['path'] do
  source   'meeseeks.env.erb'
  owner    node['meeseeks-box']['user']
  group    node['meeseeks-box']['group']
  mode     node['meeseeks-box']['env']['mode']
  variables(env_data: all_data)
  notifies :restart, 'service[meeseeks-box]', :delayed
end

file node['meeseeks-box']['config']['path'] do
  owner    node['meeseeks-box']['user']
  group    node['meeseeks-box']['group']
  mode     node['meeseeks-box']['config']['mode']
  content  node['meeseeks-box']['config']['content'].to_hash.to_yaml
  notifies :restart, 'service[meeseeks-box]', :delayed
end

directory node['meeseeks-box']['scripts']['path'] do
  owner    node['meeseeks-box']['user']
  group    node['meeseeks-box']['group']
end

git node['meeseeks-box']['scripts']['path'] do
  repository node['meeseeks-box']['scripts']['git_http']
  revision   node['meeseeks-box']['scripts']['branch']
  user       node['meeseeks-box']['user']
  action     :sync
  notifies   :restart, 'service[meeseeks-box]', :delayed
end
