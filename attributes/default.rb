# encoding: UTF-8

default['gitlab-aptly']['uri'] = 'http://aptly.gitlab.com/ci'
default['gitlab-aptly']['distribution'] = 'xenial'
default['gitlab-aptly']['components'] = %w(main stable)
default['gitlab-aptly']['arch'] = 'amd64'
default['gitlab-aptly']['key'] = 'http://aptly.gitlab.com/release.asc'
default['gitlab-aptly']['cache_rebuild'] = true
default['gitlab-aptly']['trusted'] = true

default['meeseeks-box']['package'] = 'gitlab-meeseeks-box'
default['meeseeks-box']['version'] = '0.0.26'

default['meeseeks-box']['user']  = 'meeseeks'
default['meeseeks-box']['group'] = 'meeseeks'

default['meeseeks-box']['config']['path']  = '/etc/meeseeks-box.yml'
default['meeseeks-box']['config']['mode']  = '0400'
default['meeseeks-box']['config']['content']['database']['path'] = '/var/lib/meeseeks/meeseeks.db'

default['meeseeks-box']['chef_vault']      = 'gitlab-meeseeks'
default['meeseeks-box']['chef_vault_item'] = 'prd'

default['meeseeks-box']['env']['path']    = '/etc/meeseeks-box.env'
default['meeseeks-box']['env']['mode']    = '0400'
default['meeseeks-box']['env']['content']['SLACK_TOKEN'] = 'IWouldLikeToTakeTwoStrokesOffMyGolfGame'
default['meeseeks-box']['env']['content']['CONFIG_FILE'] = '/etc/meeseeks-box.yml'

default['meeseeks-box']['scripts']['git_http'] = 'https://ops.gitlab.net/gl-infra/scripts.git'
default['meeseeks-box']['scripts']['branch']   = 'master'
default['meeseeks-box']['scripts']['path']     = '/opt/meeseeks/scripts'
