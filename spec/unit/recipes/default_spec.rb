# encoding: UTF-8

# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-meeseeks-box::default' do
  context 'when all attributes are default, on Ubuntu 16.04' do
    include ChefVault::TestFixtures.rspec_shared_context

    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04')
                            .converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'configures aptly' do
      expect(chef_run).to add_apt_repository('gitlab-aptly')
    end

    it 'installs the meeseeks package' do
      expect(chef_run).to install_apt_package('gitlab-meeseeks-box')
    end

    it 'renders the env file correctly' do
      expect(chef_run).to create_template('/etc/meeseeks-box.env').with(
        source: 'meeseeks.env.erb',
        owner: 'meeseeks',
        group: 'meeseeks',
        mode: '0400'
      )
      expect(chef_run).to render_file('/etc/meeseeks-box.env').with_content(/^SLACK_TOKEN=YourGolfGameSucks!$/)
      expect(chef_run).to render_file('/etc/meeseeks-box.env').with_content(%r{^CONFIG_FILE=\/etc\/meeseeks-box.yml$})

      t = chef_run.template('/etc/meeseeks-box.env')
      expect(t).to notify('service[meeseeks-box]').to(:restart).delayed
    end

    it 'renders the config file correctly' do
      expect(chef_run).to create_file('/etc/meeseeks-box.yml').with(
        owner: 'meeseeks',
        group: 'meeseeks',
        mode: '0400',
        content: "---\ndatabase:\n  path: \"/var/lib/meeseeks/meeseeks.db\"\n"
      )
      f = chef_run.file('/etc/meeseeks-box.yml')
      expect(f).to notify('service[meeseeks-box]').to(:restart).delayed
    end

    it 'creates the scripts folder' do
      expect(chef_run).to create_directory('/opt/meeseeks/scripts').with(
        owner: 'meeseeks',
        group: 'meeseeks'
      )
    end

    it 'syncs the scripts repo' do
      expect(chef_run).to sync_git('/opt/meeseeks/scripts')
        .with(repository: 'https://ops.gitlab.net/gl-infra/scripts.git')
        .with(revision:   'master')
    end
  end
end
