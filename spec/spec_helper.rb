# encoding: UTF-8

require 'chefspec'
require 'chefspec/berkshelf'
require 'chef-vault'

at_exit { ChefSpec::Coverage.report! }
