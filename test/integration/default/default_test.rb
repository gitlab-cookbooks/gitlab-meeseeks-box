# encoding: utf-8

# InSpec tests for recipe gitlab-meeseeks-box::default

control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab-meeseeks-box cookbook'
  desc '
    This control ensures that:
      * there is no duplicates in /etc/group'
end
